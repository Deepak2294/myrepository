package com.laundary.washingmachine.model;

public class ApiResponse {

	private String status;
	private String message;
	
	public ApiResponse() {
	}

	
	public ApiResponse(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return String.format("ApiResponse [status=%s, message=%s]", status, message);
	}
	
	

}
