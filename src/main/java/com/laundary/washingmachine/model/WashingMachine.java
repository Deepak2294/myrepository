package com.laundary.washingmachine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table
public class WashingMachine {
	@Id
	@GeneratedValue
	Integer model;

	@Column(name = "name", length = 128, unique = true, nullable = false)
	String name;

	@Column(name = "vendor", unique = false)
	String vendor;

	@Column(name = "type", nullable = true)
	String type;

	@Column(name = "price", precision = 10, scale = 2, nullable = true)
	Double price;

	@Column(name = "state", nullable = false)
	String state = "stopped";

	public WashingMachine() {

	}

	public WashingMachine(Integer model, String name, String vendor, String type, Double price, String state) {
		super();
		this.model = model;
		this.name = name;
		this.vendor = vendor;
		this.type = type;
		this.price = price;
		this.state = state;
	}

	public Integer getModel() {
		return model;
	}

	public void setModel(Integer model) {
		this.model = model;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return String.format("WashingMachine [model=%d, name=%s, vendor=%s, type=%s, price=%f, state=%s]", model, name,
				vendor, type, price, state);

	}

}
