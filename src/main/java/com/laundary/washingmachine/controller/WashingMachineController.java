package com.laundary.washingmachine.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.laundary.washingmachine.model.ApiResponse;
import com.laundary.washingmachine.model.WashingMachine;
import com.laundary.washingmachine.service.WashingMachineService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/machines")
public class WashingMachineController {

	List<String> status = Arrays.asList("Success", "failure");
	//List<String> state = Arrays.asList("stopped", "started", "paused");

	@Autowired
	WashingMachineService service;

	@Autowired
	HttpServletResponse response;

	@GetMapping("/")
	@ApiOperation(value="Find All Washing Machines",notes="Hit the API to get all the machines configured", response=WashingMachine.class)
	public List<WashingMachine> getAllMachines() {
		return service.fetchAllMachines();
	}

	@GetMapping("/{model}")
	@ApiOperation(value="Find Washing Machines by Model",notes="Provide Model to get Machine details", response=WashingMachine.class)
	public WashingMachine getMachinesByModel(@PathVariable Integer model) {
		return service.fetchMachinesByModel(model);
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/state")
	@ApiOperation(value="Find All Washing Machines States",notes="Hit the API to get all the machines states", response=List.class)
	public List<Map> getAllMachinesState() {
		return service.fetchAllMachinesState();
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/{model}/state")
	@ApiOperation(value="Find Washing Machines States by Model",notes="Provide Model to get Machine state")
	public List<Map> getMachinesStateByModel(@PathVariable Integer model) {
		return service.fetchMachinesStateByModel(model);
	}

	@PostMapping("/")
	@ApiOperation(value="Add new washing machine",notes="Provide Washing Machine details and add new washing machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> addMachine(@RequestBody WashingMachine obj)
			throws JsonProcessingException {
		WashingMachine savedObj = service.createMachine(obj);
		response.setHeader("Location", ServletUriComponentsBuilder.fromCurrentRequest().path("/{model}")
				.buildAndExpand(obj.getModel()).toUriString());
		ApiResponse apiResponse = new ApiResponse(status.get(0),
				"Machines with model " + savedObj.getName() + " is created");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.CREATED);
	}

	@PutMapping("/")
	@ApiOperation(value="Modify existing washing machine",notes="Provide Washing Machine details and update exisiting washing machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> updateMachine(@RequestBody WashingMachine obj)
			throws JsonProcessingException {
		WashingMachine updatedObj = service.updateMachineByModel(obj);
		response.setHeader("Location", ServletUriComponentsBuilder.fromCurrentRequest().path("/{model}")
				.buildAndExpand(obj.getModel()).toUriString());
		ApiResponse apiResponse = new ApiResponse(status.get(0),
				"Machines with model " + updatedObj.getModel() + " is updated");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
	}

	@PutMapping("/{model}/start")
	@ApiOperation(value="Start washing machine by Model",notes="Provide Model to start machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> startMachineByModel(@PathVariable Integer model) {
		service.turnOnMachinesByModel(model);
		ApiResponse apiResponse = new ApiResponse(status.get(0), "Machines with model " + model + " is started ");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
	}

	@PutMapping("/{model}/stop")
	@ApiOperation(value="Stop washing machine by Model",notes="Provide Model to stop machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> stopMachineByModel(@PathVariable Integer model) {
		service.turnOffMachinesByModel(model);
		ApiResponse apiResponse = new ApiResponse(status.get(0), "Machines with model " + model + " is stopped");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
	}

	@PutMapping("/{model}/pause")
	@ApiOperation(value="Pause washing machine by Model",notes="Provide Model to pause machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> pauseMachineByModel(@PathVariable Integer model) {
		service.holdMachinesByModel(model);
		ApiResponse apiResponse = new ApiResponse(status.get(0), "Machines with model " + model + " is paused");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
	}

	@DeleteMapping("/{model}")
	@ApiOperation(value="Delete washing machine by Model",notes="Provide Model to remove machine", response=ApiResponse.class)
	public ResponseEntity<ApiResponse> deleteMachinesByModel(@PathVariable Integer model) {
		service.removeMachinesByModel(model);
		ApiResponse apiResponse = new ApiResponse(status.get(0), "Machines with model " + model + " is deleted");
		return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
	}

}
