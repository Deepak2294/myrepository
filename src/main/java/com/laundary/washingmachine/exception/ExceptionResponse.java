package com.laundary.washingmachine.exception;

import java.util.Date;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionResponse {
	private String status;
	private Date timestamp;
	private String message;
	private String description;
	
	
	public ExceptionResponse() {
		
	}

	public ExceptionResponse(String status, Date timestamp, String message, String description) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.description = description;
		this.status = status;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDescription() {
		return description;
	}

	public String getStatus() {
		return status;
	}
	
	

}
