package com.laundary.washingmachine.exception;

public class InvalidObjectDefinitionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidObjectDefinitionException(String message) {
		super(message);
	}

}
