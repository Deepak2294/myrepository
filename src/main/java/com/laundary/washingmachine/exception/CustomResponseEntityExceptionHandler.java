package com.laundary.washingmachine.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ WashingMachineNotFoundException.class })
	public final ResponseEntity<ExceptionResponse> handleWashingMachineNotFoundException(
			WashingMachineNotFoundException ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse("failed",new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ InvalidObjectDefinitionException.class })
	public final ResponseEntity<ExceptionResponse> handleInvalidObjectDefinitionException(
			InvalidObjectDefinitionException ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse("failed",new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler({ WashingMachineNotModifiedException.class })
	public final ResponseEntity<ExceptionResponse> handleWashingMachineNotModifiedException(
			WashingMachineNotModifiedException ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse("failed",new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_MODIFIED);
	}
	
	@ExceptionHandler({IllegalStateException.class })
	public final ResponseEntity<ExceptionResponse> handleIllegalStateException(
			IllegalStateException ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse("failed",new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_MODIFIED);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse Response = new ExceptionResponse("failed", new Date(), "Validation Failed",
				ex.getBindingResult().toString());
		return new ResponseEntity<Object>(Response, HttpStatus.BAD_REQUEST);
	}  
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse("failed",new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	 
	

}
