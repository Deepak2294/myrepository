package com.laundary.washingmachine.exception;

public class IllegalMachineStateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalMachineStateException(String message) {
		super(message);
	}

}
