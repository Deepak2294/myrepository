package com.laundary.washingmachine.exception;

public class WashingMachineNotModifiedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public WashingMachineNotModifiedException(String message) {
		super(message);
	}

	
}
