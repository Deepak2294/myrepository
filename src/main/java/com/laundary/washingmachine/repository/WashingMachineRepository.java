package com.laundary.washingmachine.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.laundary.washingmachine.model.WashingMachine;

@Repository
public interface WashingMachineRepository extends JpaRepository<WashingMachine, Integer>{
	
	@Query("select new map(wm.name as name, wm.state as state) from WashingMachine wm")
	public List<Map> findAllNameAndState();
	
	@Query("select new map(wm.name as name, wm.state as state) from WashingMachine wm where wm.model=:m")
	public List<Map> findNameAndStateByModel(@Param("m") Integer model);
}
