package com.laundary.washingmachine.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.laundary.washingmachine.exception.IllegalMachineStateException;
import com.laundary.washingmachine.exception.InvalidObjectDefinitionException;
import com.laundary.washingmachine.exception.WashingMachineNotFoundException;
import com.laundary.washingmachine.exception.WashingMachineNotModifiedException;
import com.laundary.washingmachine.model.ApiResponse;
import com.laundary.washingmachine.model.WashingMachine;
import com.laundary.washingmachine.repository.WashingMachineRepository;

@Service
public class WashingMachineService {
	Logger log = LoggerFactory.getLogger(this.getClass());
	//List<String> status = Arrays.asList("Success", "failure");
	List<String> state = Arrays.asList("stopped", "started", "paused");

	@Autowired
	private WashingMachineRepository repository;

	// Business Logic for all get mappings
	public List<WashingMachine> fetchAllMachines() {
		log.debug("Fetching all washing machine details from database");
		List<WashingMachine> listOfMachines = repository.findAll();
		log.debug("Data found in database []", listOfMachines);
		List<WashingMachine> allMachines =  Optional.ofNullable(listOfMachines).orElseThrow(
				() -> new WashingMachineNotFoundException("There is no washing machine installed currenlty"));
		if (allMachines.isEmpty())
			throw new WashingMachineNotFoundException("There is no washing machine installed currenlty");
		return allMachines;
	}

	public WashingMachine fetchMachinesByModel(Integer model) {
		log.debug("Fetching washing machine with " + model + "  from database");
		return repository.findById(model).orElseThrow(
				() -> new WashingMachineNotFoundException("Washing machine with Model " + model + " does not exists"));
	}

	@SuppressWarnings("rawtypes")
	public List<Map> fetchAllMachinesState() {
		log.debug("Fetching all washing machine by their current state from database");
		List<Map> stateByModel = Optional.ofNullable(repository.findAllNameAndState()).orElseThrow(
				() -> new WashingMachineNotFoundException("There is no washing machine installed currenlty"));
		if (stateByModel.isEmpty())
			throw new WashingMachineNotFoundException("There is no washing machine installed currenlty");
		return stateByModel;
	}

	@SuppressWarnings("rawtypes")
	public List<Map> fetchMachinesStateByModel(Integer model) {
		log.debug("Fetching washing machine and its current state with " + model + " from database");
		List<Map> stateByModel = Optional.ofNullable(repository.findNameAndStateByModel(model)).orElseThrow(
				() -> new WashingMachineNotFoundException("Washing machine with model " + model + " does not exist"));
		if (stateByModel.isEmpty())
			throw new WashingMachineNotFoundException("Washing machine with model " + model + " does not exist");
		return stateByModel;
	}

	// Business Logic for Post Mapping
	public WashingMachine createMachine(WashingMachine obj) throws JsonProcessingException {
		log.debug("Saving Washing machine object into database");

		if (!state.contains(obj.getState().toLowerCase()))
			throw new IllegalMachineStateException("Washing machine valid allowed states " + state);
		
		if (!obj.getState().equalsIgnoreCase(state.get(0)))
			throw new IllegalMachineStateException("Washing machine state must be Stopped");

		repository.save(Optional.ofNullable(obj)
				.orElseThrow(() -> new InvalidObjectDefinitionException("Invaild Washing Machine object")));
		log.debug("Washing machine object saved in database successfully", obj);
		return obj;
	}

	// Business Logic for Put Mapping
	public WashingMachine updateMachineByModel(WashingMachine obj) throws JsonProcessingException {
		Integer fetchedModel = Optional.ofNullable(obj.getModel()).orElseThrow(
				() -> new WashingMachineNotFoundException("Can not update Washing machine as Model is not defined"));

		if (!state.contains(obj.getState().toLowerCase()))
			throw new IllegalMachineStateException("Washing machine valid allowed states " + state);

		if (obj.getState().equalsIgnoreCase(state.get(2)))
			throw new IllegalMachineStateException("Washing machine state can only be Stopped/Started");

		WashingMachine existingObj = repository.findById(fetchedModel)
				.orElseThrow(() -> new WashingMachineNotModifiedException(
						"Washing machine with Model " + fetchedModel + " does not exists"));

		if (!existingObj.getState().equalsIgnoreCase(state.get(0)))
			throw new IllegalStateException(
					"Washing machine is Started/Paused, Kindly stop the machine before updating");

		existingObj.setName(obj.getName());
		existingObj.setPrice(obj.getPrice());
		existingObj.setState(obj.getState());
		existingObj.setType(obj.getType());
		existingObj.setVendor(obj.getVendor());

		log.debug("Updaing Washing machine with model " + obj.getModel() + " object into database");
		repository.save(Optional.ofNullable(existingObj)
				.orElseThrow(() -> new InvalidObjectDefinitionException("Invaild Washing Machine object")));
		log.debug("Washing machine object updated in database successfully {}", existingObj);
		return existingObj;
	}

	public void turnOnMachinesByModel(Integer model) {
		WashingMachine existingObj = repository.findById(model)
				.orElseThrow(() -> new WashingMachineNotModifiedException(
						"Washing machine with Model " + model + " does not exists"));
		log.debug("Washing machine with model " + model + " is starting..");
		if (existingObj.getState().equalsIgnoreCase(state.get(1)))
			throw new IllegalMachineStateException("washing machines is already running..");		
		existingObj.setState(state.get(1));
		repository.save(existingObj);
	}

	public void turnOffMachinesByModel(Integer model) {
		WashingMachine existingObj = repository.findById(model)
				.orElseThrow(() -> new WashingMachineNotModifiedException(
						"Washing machine with Model " + model + " does not exists"));
		log.debug("Washing machine with model " + model + " is stopping..");
		if (existingObj.getState().equalsIgnoreCase(state.get(0)))
			throw new IllegalMachineStateException("Washing machines is already stopped..");
		existingObj.setState(state.get(0));
		repository.save(existingObj);
	}

	public void holdMachinesByModel(Integer model) {
		WashingMachine existingObj = repository.findById(model)
				.orElseThrow(() -> new WashingMachineNotModifiedException(
						"Washing machine with Model " + model + " does not exists"));
		log.debug("Washing machine with model " + model + " is paused..");
		if (existingObj.getState().equalsIgnoreCase(state.get(2)))
			throw new IllegalMachineStateException("Washing machine is already paused..");
		existingObj.setState(state.get(2));
		repository.save(existingObj);
	}

	// Business Logic for delete Mapping
	public void removeMachinesByModel(Integer model) {
		WashingMachine existingObj = repository.findById(model)
				.orElseThrow(() -> new WashingMachineNotModifiedException(
						"Washing machine with Model " + model + " does not exists"));
		if (!existingObj.getState().equalsIgnoreCase(state.get(0)))
			throw new IllegalMachineStateException(
					"Washing machine is Started/Paused, Kindly stop the machine before updating");
		log.debug("Washing machine with model " + model + " is deleting");
		repository.deleteById(model);
	}

}
