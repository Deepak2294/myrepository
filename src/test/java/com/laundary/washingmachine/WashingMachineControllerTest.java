package com.laundary.washingmachine;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.laundary.washingmachine.controller.WashingMachineController;
import com.laundary.washingmachine.model.WashingMachine;
import com.laundary.washingmachine.service.WashingMachineService;

@WebMvcTest(controllers = WashingMachineController.class)
public class WashingMachineControllerTest {

	@MockBean
	private WashingMachineService service;

	@Autowired
	MockMvc mockMvc;
	
	@Test
	@DisplayName("Get all machines object from database")
	public void getAllMachinesTest() throws Exception {
		List<WashingMachine> wmList = new ArrayList<WashingMachine>();
		wmList.add(new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90, "stopped"));
		wmList.add(new WashingMachine(102, "Voltas-Washer", "Voltas Appliances", "semi-automated", 12000.00, "stopped"));
		
		Mockito.when(service.fetchAllMachines()).thenReturn(wmList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/machines/"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].model", Matchers.is(101)))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("LG-Dom-Washer")))
		.andExpect(MockMvcResultMatchers.jsonPath("$[1].model", Matchers.is(102)))
		.andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is("Voltas-Washer")));
		
	}
	
	@Test
	@DisplayName("Get machines object by model from database")
	public void getMachinesByModelTest() throws Exception {
		WashingMachine obj1 = new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90, "stopped");
		
		Mockito.when(service.fetchMachinesByModel(101)).thenReturn(obj1);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/machines/101"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.jsonPath("model", Matchers.is(101)))
		.andExpect(MockMvcResultMatchers.jsonPath("name", Matchers.is("LG-Dom-Washer")));
		
	}
}
