package com.laundary.washingmachine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.laundary.washingmachine.exception.IllegalMachineStateException;
import com.laundary.washingmachine.exception.WashingMachineNotFoundException;
import com.laundary.washingmachine.model.WashingMachine;
import com.laundary.washingmachine.repository.WashingMachineRepository;
import com.laundary.washingmachine.service.WashingMachineService;

@SpringBootTest
public class WashingMachineServiceTest {

	@Autowired
	WashingMachineService service;

	@MockBean
	private WashingMachineRepository repository;

	@MockBean
	WashingMachine wmObj;

	public WashingMachineServiceTest() {
		// TODO Auto-generated constructor stub
	}

	@Test
	@DisplayName("Test to fetch all the Machines configured")
	void fetchAllMachinesTest() {
		// Configure Mock
		List<WashingMachine> wmList = new ArrayList<WashingMachine>();
		wmList.add(new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90, "stopped"));
		wmList.add(
				new WashingMachine(102, "Voltas-Washer", "Voltas Appliances", "semi-automated", 12000.00, "stopped"));
		doReturn(Arrays.asList(wmList)).when(repository).findAll();

		// Execute Service Call
		List<WashingMachine> returnedMachine = service.fetchAllMachines();

		// Assert the response
		Assertions.assertNotNull(returnedMachine, "Returned machine list should not be null");
		Assertions.assertEquals(2, wmList.size(), "find All should return two Machine objects");
	}

	@Test
	@DisplayName("Test to fetch all the Machines and throw exception if empty or null is returned")
	void fetchAllMachinesThrowExceptionTest() {
		// Test if Null is returned
		doReturn(null).when(repository).findAll();
		WashingMachineNotFoundException exception1 = assertThrows(WashingMachineNotFoundException.class, () -> {
			service.fetchAllMachines();
		});
		assertTrue(exception1.getMessage().contains("There is no washing machine installed currenlty"));

		// Test if Empty list is return
		List<WashingMachine> wmList = Arrays.asList();
		doReturn(wmList).when(repository).findAll();
		WashingMachineNotFoundException exception2 = assertThrows(WashingMachineNotFoundException.class, () -> {
			service.fetchAllMachines();
		});
		assertTrue(exception2.getMessage().contains("There is no washing machine installed currenlty"));
	}

	@Test
	@DisplayName("Test to fetch Machines by model")
	void fetchMachinesByModelTest() {
		// Configure Mock
		WashingMachine wm = new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90,
				"stopped");
		doReturn(Optional.of(wm)).when(repository).findById(101);

		// Execute Service Call
		WashingMachine returnedMachine = service.fetchMachinesByModel(101);

		// Assert the response
		Assertions.assertNotNull(returnedMachine, "returned object must not be null");
		Assertions.assertSame(wm, returnedMachine, "Input and output object should be same");
	}

	@Test
	@DisplayName("Test to fetch Machines by model and throw exception if machine not found")
	void fetchMachinesByModelThrowsExceptionTest() {
		// Configure Mock
		WashingMachine wmObj = null;
		Integer model = 101;
		doReturn(Optional.ofNullable(wmObj)).when(repository).findById(model);

		// Execute Service Call
		WashingMachineNotFoundException exception = assertThrows(WashingMachineNotFoundException.class, () -> {
			service.fetchMachinesByModel(model);
		});
		// Assert the response
		assertTrue(exception.getMessage().contains("Washing machine with Model " + model + " does not exists"));
	}

	@Test
	@DisplayName("Test to fetch Machines by state")
	void fetchAllMachinesStateTest() {
		// Configure Mock
		Map state = new HashMap();
		state.put("LG-Washer", "Stopped");
		state.put("Voltas-Washer", "started");
		List<Map> stateList = Arrays.asList(state);

		doReturn(stateList).when(repository).findAllNameAndState();

		// Execute Service Call
		List<Map> returnStateList = service.fetchAllMachinesState();

		// Assert the response
		assertEquals(returnStateList, stateList, "Return list of all Machine state & input list should be same");
	}

	@Test
	@DisplayName("Test to fetch Machines by state and throw exception if returned empty or null")
	void fetchAllMachinesStateThrowExceptionTest() {
		doReturn(null).when(repository).findAllNameAndState();
		WashingMachineNotFoundException exception1 = assertThrows(WashingMachineNotFoundException.class, () -> {
			service.fetchAllMachinesState();
		});
		assertTrue(exception1.getMessage().contains("There is no washing machine installed currenlty"));

		doReturn(Arrays.asList()).when(repository).findAllNameAndState();
		WashingMachineNotFoundException exception2 = assertThrows(WashingMachineNotFoundException.class, () -> {
			service.fetchAllMachinesState();
		});
		assertTrue(exception2.getMessage().contains("There is no washing machine installed currenlty"));
	}

	@Test
	@DisplayName("Test to fetch Machines state by Model")
	void fetchMachinesStateByModelTest() {
		// Configure Mock
		Map state = new HashMap();
		state.put("LG-Washer", "Stopped");
		List<Map> stateList = Arrays.asList(state);
		doReturn(stateList).when(repository).findNameAndStateByModel(101);
		List<Map> returnStateList = service.fetchMachinesStateByModel(101);
		assertEquals(returnStateList, stateList, "Return list of all Machine state & input list should be same");
	}

	@Test
	@DisplayName("Test to add a new Machines")
	void createMachineTest() throws Exception {
		WashingMachine wm = new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90,
				"stopped");
		doReturn(wm).when(repository).save(wm);
		WashingMachine returnedObj = service.createMachine(wm);
		assertEquals(wm.getModel(), returnedObj.getModel(), "Obth object should be equal");
	}

	@Test
	@DisplayName("Test to add a new Machines and throw exception if returned empty or null")
	void createMachineThrowExceptionTest() {
		WashingMachine wmObj1 = new WashingMachine(101, "Voltas-All-Washer", "Voltas", "fully-automated", 10000.90,
				"halted");
		WashingMachine wmObj2 = new WashingMachine(101, "LG-Dom-Washer", "Life's Good", "fully-automated", 10000.90,
				"started");
		
		List<String> validStates = Arrays.asList("stopped","started","paused");
		
		//If invalid state is passed
		IllegalMachineStateException exception1 = assertThrows(IllegalMachineStateException.class, () -> {
			service.createMachine(wmObj1);
		});
		assertTrue(exception1.getMessage().contains("Washing machine valid allowed states " + validStates));
		
		//if machine state is other than stopped while creating object
		IllegalMachineStateException exception2 = assertThrows(IllegalMachineStateException.class, () -> {
			service.createMachine(wmObj2);
		});
		assertTrue(exception2.getMessage().contains("Washing machine state must be Stopped"));
	}

}
